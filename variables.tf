variable "zone" {
    type = string
    default = ""
}

variable "ttl" {
    type = string
    default = "3600"
}

variable "create_naked" {
    type = bool
    default = false
}

variable "create_zone" {
    type = bool
    default = false
}

variable "naked_records" {
    type = list(string)
    default = []
}

variable "hosts" {
  type = map(object({
    name    = string
    type    = string
    ttl     = string
    records = list(string)
  }))

  default = {}
}

variable "hosts_with_alias" {
    type = map(object({
        name    = string,
        type    = string,
        ttl     = string,
        alias   = map(any)
    }))
    default = {}
}
