resource "aws_route53_zone" "domain" {
    count = var.create_zone ? 1 : 0 
    name  = var.zone
}

resource "aws_route53_record" "naked_host" {
    count   = var.create_zone && var.create_naked ? 1 : 0 
    zone_id = aws_route53_zone.domain[0].id
    name    = var.zone
    type    = "A"
    ttl     = var.ttl
    records = var.naked_records
}

resource "aws_route53_record" "hosts" {

    for_each = var.create_zone ? var.hosts : {}

    zone_id   = aws_route53_zone.domain[0].id
    name      = each.value.name
    type      = each.value.type
    ttl       = each.value.ttl
    records   = each.value.records
}

resource "aws_route53_record" "hosts_with_alias" {

    for_each = var.create_zone ? var.hosts_with_alias : {}

    zone_id = aws_route53_zone.domain[0].id
    name    = each.value.name
    type    = each.value.type
    alias   {
        name                   = each.value.alias["name"]
        zone_id                = each.value.alias["zone_id"]
        evaluate_target_health = each.value.alias["evaluate_target_health"]
    } 
}

    