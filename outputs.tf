output "name_servers" {
    value = aws_route53_zone.domain[0].name_servers 
}

output "zone_id" {
    value = aws_route53_zone.domain[0].zone_id
}
